<?php get_header(); ?>
<?php
    $text_under_video = get_field('text_under_video');
    $q_a = get_field('q_a');
    $video_link = get_field('video');
	
	$lang=pll_current_language();
?>
<html>
<head>
<script> 
        // wait for the DOM to be loaded 
        $(document).ready(function() {
			$('.form__item').keyup(function(){
				$(this).removeClass('required');
				$('.search__error').hide();
			});

            $('#myForm').ajaxForm({
                method: 'post',
                dataType:'json',
                beforeSubmit: function( formData, jqForm, options ){
                    $('.form_loader').show();
                    $('#myForm .form__row').css('opacity', '0.1');
                    
                    $('.form__main').find('.form__item').removeClass('required');
                    $('.form__container_form').find('.search__error').hide();
                },
                success: function(response){
                    $('.form_loader').hide();
                    $('#myForm .form__row').css('opacity', '1');
                    
                    if(response.status)
                    {
                        $('.required').removeClass('required');
                        $('.search__error').hide();
                        $('#myForm')[0].reset();
                        $('.search__error_success').text( response.message.text ).show();

                    }
                    else
                    {
                        if( response.message.field ){
                            $('.form__main').find('[name="' + response.message.field + '"]').addClass('required');
                        }
                        
                        $('.search__error_required').text( response.message.text ).show();
                    }

                },

            });

            
            
            
            // Video player start
            var player = function(element){
                var self = this;
                
                this.urls = [];
                this.video_json = <?php echo json_encode($video_link); ?>;
                this.videoContainer = jQuery(element);
                this.videoPlayer = null;
                this.videoButton = null;
                this.currentVideo = 0;
                this.clicked = true;
                this.options = {
                    volume: 0,
                    height: '480px',
                    width: '640px',
                    align: 'center',
                };
                
                this.collectUrls = function(){
                    for (i in this.video_json){
                        this.urls.push(this.video_json[i].video.url);
                    }
                }
                this.getVideoLink = function(){
                    if (typeof(this.urls[this.currentVideo]) == 'undefined'){
                        this.currentVideo = 0;
                    }
                    var link = this.urls[this.currentVideo] || '';
                    this.currentVideo++;
                    
                    return link;
                }
                this.mergeOptions = function(newoptions){
                    for (i in newoptions){
                        this.options[i] = newoptions[i];
                    }
                }
                
                this.init = function(){
                    var newoptions = arguments[0] || {};
                    this.mergeOptions(newoptions);
                    
                    if (!this.videoPlayer){
                        // Add play button
                        this.videoButton = jQuery('<img />', {
                            src: '<?php echo get_template_directory_uri().'/img/playbtn.png'?>',
                        });
                        this.videoButton.css({
                            position: 'absolute', 
                            height: '100px', 
                            width: '100px',
                            left: '50%',
                            top: '50%',
                            transition: 'opacity .3s ease',
                            'margin-left': '-50px',
                            'margin-top': '-50px',
                            'pointer-events': 'none',
                        });
                        this.videoContainer.append(this.videoButton);
                        
                        this.collectUrls();
                        this.videoPlayer = jQuery('<video />', {
                            src: this.getVideoLink() + '#t=0.1', // add thumbnail image
                            controls: false, 
                            preload: 'auto', 
                        });
                        this.videoPlayer.get(0).volume = this.options.volume;
                        
                        this.videoPlayer.on('ended', function(){
                            this.src = self.getVideoLink();
                            this.play();
                        });
                        
                        this.videoPlayer.on('click', function(){
                            if (self.clicked){
                                self.videoButton.hide();
                                self.clicked = false;
                                this.play();
                            } else {
                                self.videoButton.show();
                                self.clicked = true;
                                this.pause();
                            }                            
                        });
                        
                        this.videoPlayer.on('mouseenter', function(){
                            self.clicked ? this.play() : '';
                            self.videoButton.css('opacity', 0.5);
                        });
                        this.videoPlayer.on('mouseleave', function(){
                            self.clicked ? this.pause() : '';
                            self.videoButton.css('opacity', 1);
                        });
                        
                        this.videoContainer.append(this.videoPlayer);
                    }
                    
                    this.videoPlayer.css(this.options),
                    this.videoContainer.css({width: '100%', height: '100%', 'text-align': this.options.align});
                }
            }
            
            
            video_player = new player('.video_player_container');
            video_player.init({
                height: '100%',
                width: '100%',
            });
            // Video player end
        }); 
        
    </script> 
</head>
<body>
	<!-- VIDEO START -->
	<section class="row video">
		<div class="col-xs-12 video__container-fix">
			<div class="flex__container video__container">
                <div class="video_player_container"></div>
            </div>
		</div>
	</section>
	<!-- VIDEO END -->

	<!-- FORM START -->
	<section id="form" class="row form">
		<div class="col-xs-12 form__container-fix">
			<div class="flex__container form__container">
				<div class="form__container_desc">
					<div class="form__container_desc-heading"><h1><?php echo __($text_under_video[0]["header"] ,'cloud4u')  ?></h1></div>
					<div class="form__container_desc-text"><p><? echo $text_under_video[0]["text"]  ?></p></div>
					
					<div class="form__container_form">
						<form class="form__main" id="myForm" action="<?php echo site_url('wp-admin/admin-ajax.php') ?>" method="post">
							<input type="hidden" name="action"  value="contact" >
                            
                            <div class="form_loader" style="display: none;">
                                <div id="circleG">
                                    <div id="circleG_1" class="circleG"></div>
                                    <div id="circleG_2" class="circleG"></div>
                                    <div id="circleG_3" class="circleG"></div>
                                </div>
                            </div>
                            
							<div class="form__row form__row--1">
								<input class="form__item form__item--1" type="text" placeholder="<?php echo get_field('placeholder_name_'.$lang, 'options') ?>" name="name">
								<input class="form__item form__item--2" type="text" placeholder="<?php echo get_field('placeholder_email_'.$lang, 'options') ?>" name="email">
							</div>
							<div class="form__row form__row--2">
								<input class="form__item form__item--3" type="text" placeholder="<?php echo get_field('placeholder_subject_'.$lang, 'options') ?>" name="subject">
							</div>
							<div class="form__row form__row--3">
								<textarea class="form__item form__item--4" placeholder="<?php echo get_field('placeholder_message_'.$lang, 'options') ?>" name="message"></textarea>
							</div>
							<button type="submit" class="form__row form__row--4 button"><? echo get_field('placeholder_send_'.$lang, 'options') ?></button>
                        </form>
						
						<!-- error start -->

						<div class="form__row form__row--5 search__error search__error_mail"><?php echo get_field('valid_email_'.$lang, 'options') ?></div>
						<div class="form__row form__row--6 search__error search__error_required"><?php echo get_field('required_fields_'.$lang, 'options') ?></div>
						<div class="form__row form__row--7 search__error search__error_success"><?php echo get_field('success_'.$lang, 'options') ?></div>

						<!-- error end -->

					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- FORM END -->

	<!-- QNA START -->

	<section id="qna" class="row qna">
		<div class="col-xs-12 qna__container-fix">
			<div class="flex__container qna__container">
				<div class="qna__container_heading">
					<div class="qna__heading-h1"><h1><?php echo __('Frequently asked questions', 'cloud4u') ?></h1></div>
					<div class="qna__search">
						<div class="search-field">
							<form action="" class="search__form">
								<input type="search" placeholder="<?php echo __('What are you looking for?', 'cloud4u') ?>">
							</form>
							<div class="search-icon"></div>
						</div>
					</div>
				</div>
				
				<? foreach($q_a as $qa): ?>
					<div class="qna__container_question">
						<div class="qna__quest_heading">
							<div class="qna__heading-h2"><h2><? echo __($qa["question"], 'cloud4u')  ?></h2></div>
							<div class="qna__cut"></div>
						</div>
						<div class="qna__quest_text"><p><? echo __($qa["answer"], 'cloud4u')  ?></p></div>
					</div>
				<? endforeach ?>

			</div>
		</div>
	</section>

	<!-- QNA END -->
</body>
</html>
<?php get_footer(); ?>