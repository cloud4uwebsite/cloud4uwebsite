<?php


if (!is_admin()) {
    add_action( 'wp_enqueue_scripts', 'frontend_scripts' );
}

function frontend_scripts() {
    
    wp_deregister_script('jquery');
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.js' );
	wp_enqueue_script('jquery');

	wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/js/jquery-ui.js' );
	wp_enqueue_script( 'vimeo', get_template_directory_uri() . '/js/vimeowrap.js' );
	wp_enqueue_script( 'ajax-form', 'http://malsup.github.com/jquery.form.js');
    
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.js' );
	wp_enqueue_script( 'bootstrap-min-js', get_template_directory_uri() . '/js/bootstrap.min.js' );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js' );
    
	wp_enqueue_style( 'reset', get_template_directory_uri() . '/css/reset.css' );
	$lang=pll_current_language();
	if($lang=='he')
	{
		wp_enqueue_style( 'rtl', get_template_directory_uri() . '/css/style.rtl.css' );
	}
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style( 'bootstrap-min', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'jquery-ui-css', get_template_directory_uri() . '/css/jquery-ui.css' );
	wp_enqueue_style( 'jquery-ui-min-css', get_template_directory_uri() . '/css/jquery-ui.min.css' );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css' );
	wp_enqueue_style( 'font-1', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic' );
	wp_enqueue_style( 'font-2', 'https://www.fontify.me/wf/f4f8bd2986860ed24a046d59ec5ee844' );
}


add_action( 'admin_enqueue_scripts', 'update_scripts' );
function update_scripts() {
    wp_deregister_script('jquery-ui-position');
	wp_enqueue_script( 'jquery-ui-position', get_template_directory_uri() . '/js/jquery-ui/jquery-ui-position.js', array('jquery'), '1.12.1', 1 );
	wp_enqueue_script('jquery-ui-position');

}


add_action( 'wp_ajax_contact', 'contact' );
add_action( 'wp_ajax_nopriv_contact', 'contact' );
function contact() {
    $lang = pll_current_language();
    $name = $_POST['name'];
    
    if(empty($name))
    {
        $error_text = get_field('required_fields_'.$lang, 'options');
        $response = array(
            'status' => false,
            'message'=> array(
                'field' => 'name',
                'text' => $error_text,
            )
        );

        exit( json_encode($response) );
    }
    
    $email = $_POST['email'];
    if(empty($email))
    {
        $error_text = get_field('required_fields_'.$lang, 'options');
        $response = array(
            'status' => false,
            'message'=> array(
                'field' => 'email',
                'text' => $error_text,
            ) 
        );
        
        exit( json_encode($response) );
    }
    
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
    {
        $error_text = get_field('valid_email_'.$lang, 'options');
        $response = array(
            'status' => false,
            'message'=> array(
                'field' => 'email',
                'text' => $error_text,
            )
        );
        exit( json_encode($response) );
    }
    
    $subject = $_POST['subject'];
    $message = $_POST['message'];
    //$message=$name.','.$email.'. '.$message;
    $message_text = get_field('feedback_'.$lang, 'options');
    $message_text = preg_replace('/\:\[email\]\:/', $email, $message_text);
    $message_text = preg_replace('/\:\[name\]\:/', $name, $message_text);
    $message_text = preg_replace('/\:\[subject\]\:/', $subject, $message_text);
    $message_text = preg_replace('/\:\[message\]\:/', $message, $message_text);
    $mail_to = get_field('email_to_send', 'options');
    $headers = 'Content-type: text/html; charset=utf-8';
    if( !wp_mail($mail_to, $subject, $message_text, $headers) ){
        $error_text = get_field('error_when_send_email_'.$lang, 'options');
        $response = array(
            'status' => false,
            'message'=> array(
                'field' => '',
                'text' => $error_text,
            )
        );
        exit( json_encode($response) );
    };

    $success_text = get_field('success_'.$lang, 'options');
    $response = array(
        'status' => true,
        'message'=> array(
            'field' => '',
            'text' => $success_text,
        )
    );
        
    exit( json_encode($response) ); 

}

function lang_setup(){
        load_theme_textdomain( 'cloud4u', get_template_directory() );
    }
    add_action('after_setup_theme', 'lang_setup');

    if ( function_exists('acf_add_options_page') ) {
    
	    acf_add_options_page(array(
	        'page_title'    => 'Common content options',
	        'menu_title'    => 'Common content options',
	        'menu_slug'     => 'theme-common-settings',
	        'capability'    => 'edit_posts',
	        'position'      => 40.1,
	        'icon_url'      => 'dashicons-format-aside',
	        'redirect'      => true
	    ));
	    
	    acf_add_options_sub_page(array(
	        'page_title'    => 'Header options',
	        'menu_title'    => 'Header options',
	        'parent_slug'   => 'theme-common-settings',
	        'menu_slug'     => 'theme-header-settings',
	    ));

	    acf_add_options_sub_page(array(
	        'page_title'    => 'Footer options',
	        'menu_title'    => 'Footer options',
	        'parent_slug'   => 'theme-common-settings',
	        'menu_slug'     => 'theme-footer-settings',
	    ));
		
		acf_add_options_sub_page(array(
	        'page_title'    => 'Contact form options',
	        'menu_title'    => 'Contact form options',
	        'parent_slug'   => 'theme-common-settings',
	        'menu_slug'     => 'theme-contact-form-settings',
	    ));
	}

function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

?>