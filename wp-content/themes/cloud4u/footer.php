	<!-- FOOTER START -->

		<footer class="row footer">
			<div class="col-xs-12 footer__container-fix">
				<div class="flex__container footer__container">
					<div class="footer__container_copyright">
					<?php $lang = pll_current_language();
						echo get_field('footer_text_' . $lang, 'options'); 
					?>	
					</div>
				</div>
			</div>
		</footer>

	<!-- FOOTER END -->

<?php wp_footer(); ?>

	
