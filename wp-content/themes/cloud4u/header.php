<?php 
    $favicon = get_field('favicon', 'options');
?>
<title><?php echo get_the_title();?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">
<link rel="icon" href="<?php echo $favicon['url'] ?>" type="<?php echo $favicon['mime_type']?>">
<link rel="shortcut icon" href="<?php echo $favicon['url'] ?>" type="<?php echo $favicon['mime_type']?>">
<?php wp_head(); ?>

	<!-- HEADER START --> 
    <?php $langs=pll_the_languages(array('raw'=>1, 'force_home' => 1)); ?>
	<?php $lang=pll_current_language() ?>	
    
	<header id="header" class="row header">
		<div class="col-xs-12 header__container-fix">
			<div class="flex__container header__container">
				<a class="header__logo_link" href="<?php echo $langs[ $lang ]['url'] ?>">
				<?php $logo = get_field('logo', 'options'); ?>
					<div class="header__container_logo" style="background-image: url(<?php echo $logo['url']; ?>);"></div>
				</a>
				<div class="header__container_menu">
					<div class="header__container_menu-item lang_box">

						<?php $lang1=pll_the_languages(array('raw'=>1, 'force_home' => 1)); ?>
						<?php $lang=pll_current_language() ?>

						<?php if($lang=='en'): ?>

							<a href="#"><div class="header__item_lang lang--en lang-current"></div></a><!-- if language is EN [default] set as current if not set as hidden -->
							<a href="<?php echo $langs['he']['url'] ?>"><div class="header__item_lang lang--is lang-hidden"></div></a><!-- if language is HEB set as current if not set as hidden-->
						<?php elseif($lang=='he'): ?>
							<a href="#"><div class="header__item_lang lang--is lang-current"></div></a><!-- if language is EN [default] set as current if not set as hidden -->
							<a href="<?php echo $langs['en']['url'] ?>"><div class="header__item_lang lang--en lang-hidden"></div></a><!-- if language is HEB set as current if not set as hidden-->
						<?php endif ?>
					</div>
					<a class="link header__menu_link header_link current-link" href="#header">
						<div class="header__container_menu-item header__item_link">
						<?php echo get_field('home_' . $lang, 'options') ?>
						</div>
					</a>
					<a class="link header__menu_link qna_link" href="#qna">
						<div class="header__container_menu-item header__item_link">
						<?php echo get_field('qna_' . $lang, 'options') ?>
						</div>
					</a>
					<a class="link header__menu_link form_link" href="#form">
						<div class="header__container_menu-item header__item_link">
						<?php echo get_field('contact_us_' . $lang, 'options') ?>
						</div>
					</a>
					<div class="header__menu_wrapper">
						<img src="<?php echo get_template_directory_uri()?>/img/menu_mob.png" alt="" class="header__wrapper-img">
						<img src="<?php echo get_template_directory_uri()?>/img/menu_mob-off.png" class="header__wrapper-img-off">
					</div>
				</div>
                <div class="header__menu_mob">
                    <a class="header_link mob_link current-link" href="#header">
                        <div class="header__mob_item mob__header_item">
                        <?php echo get_field('home_' . $lang, 'options') ?>
                        </div>
                    </a>
                    <a class="qna_link mob_link" href="#qna">
                        <div class="header__mob_item mob__pnc_item">
                        <?php echo get_field('qna_' . $lang, 'options') ?>
                        </div>
                    </a>
                    <a class="form_link mob_link" href="#form">
                        <div class="header__mob_item mob__form_item">
                        <?php echo get_field('contact_us_' . $lang, 'options') ?>
                        </div>
                    </a>
                </div>
			</div>
		</div>
	</header>
	<!-- HEADER END -->