//SCRIPTS START

// hide controls

//lang start

    $(document).ready(function() {

        $('.lang-current').on("click", function(speed, callback){
            $('.lang-hidden').addClass('lang-up').toggle('slide', {direction: 'down'}, speed, callback);
        });
        $('.lang-hidden').on("click", function() {
            $('.lang-current').addClass('lang-hidden').removeClass('lang-current');
            $(this).removeClass('lang-up').removeClass('lang-hidden').addClass('lang-current');
        });
    });


	//reloading page after that script with style.rtl.css instead of style.css -->

	//lang end

	// wrapper start

	$(document).ready(function() {
		$('.header__menu_wrapper').on('click', function(speed, callback) {
			$('.header__menu_mob').slideToggle(100, function() {
				$('.header__wrapper-img').toggle();
				$('.header__wrapper-img-off').toggle();
			});
		});
		$('.mob_link').on('click', function(speed, callback) {
			$('.header__menu_mob').slideUp(function() {
				$('.header__wrapper-img').toggle();
				$('.header__wrapper-img-off').toggle();
			});
		});
	});

	// wrapper end

	//link start

	
		$(document).ready(function() {
			$('.qna_link').on("click", function(speed, callback) {
				$(this).addClass('current-link');
				$('.header_link').removeClass('current-link');
				$('.form_link').removeClass('current-link');
				$('.video').hide();
				$('.form').hide();
				$('.qna').show('fade');
				return false;
			});
			$('.header_link').on("click", function(speed, callback) {
				$(this).addClass('current-link');
				$('.qna_link').removeClass('current-link');
				$('.form_link').removeClass('current-link');
				$('.qna').hide();
				$('.video').show('fade');
				$('.form').show('fade');
			});
			$('.form_link').on("click", function(speed, callback) {
				$(this).addClass('current-link');
				$('.header_link').removeClass('current-link');
				$('.qna_link').removeClass('current-link');
				$('.qna').hide();
				$('.video').show();
				$('.form').show('fade');
			});
		});


	//link end

	//search start

		$(document).ready(function() {
	        $('.search-icon').on("click", function(speed,callback){
	            $('.search__form').toggle('slide', {direction: 'right'}, speed, callback);
	            $(this).toggleClass("search-icon--toggle");
	            return false;
	        });
		});


	//search end

	//question start

		$(document).ready(function() {
	        $('.qna__heading-h2').on("click", function(e){
	            $(this).parent().parent('.qna__container_question').toggleClass('.current-question').children('.qna__quest_text').toggle('fade');
	           // $('.qna__cut').toggleClass('qna__cut-reverse');
				$(this).parent().find('.qna__cut').toggleClass('qna__cut-reverse');//.parent().parent('.qna__container_question').toggleClass('.current-question').children('.qna__quest_text').toggle('fade');
				return false;
	        });
		});
		
		
		$(document).ready(function() {
	        $('.qna__cut').on("click", function(e){
	            $(this).toggleClass('qna__cut-reverse').parent().parent('.qna__container_question').toggleClass('.current-question').children('.qna__quest_text').toggle('fade');
	            return false;
	        });
		});


	//question end

	$(document).ready(function() {
		$(window).trigger('resize');
	});

	$(window).resize(function () {
		var h = $('#wpadminbar').outerHeight();
		if($('#wpadminbar').length) {
			$('.header__menu_wrapper').css('top', -128 + h);
			$('.header__menu_mob').css('top', 55 + h);
			$('.header__container-fix').css('margin-top', h);
		}; 
		
		var winBr = $(window).innerWidth();
	    if (winBr > 510) {
	    	$('.header__menu_mob').slideUp(function() {
				$('.header__wrapper-img').show();
				$('.header__wrapper-img-off').hide();
			});
	        $('.header__menu_mob').hide();
	    }
		else {
			$('.header__container-fix').css('margin-top', 0);
		}
	});


	//SCRIPTS END